function updatePrice() {
    let radio = document.getElementById("radios");
    let check = document.getElementById("checkboxes");
    let s = document.getElementsByName("prodType");
    let count = document.getElementById("count");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }

    if (select.value === "2") {
        radio.style.display = "block";
    } else {
        radio.style.display = "none";
    }
    if (select.value === "3") {
        check.style.display = "block";
    } else {
        check.style.display = "none";
    }

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
        if (radio.checked) {
            let optionPrice = prices.prodOptions[radio.value];
            if (optionPrice !== undefined) {
                price += optionPrice;
            }
        }
    });

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
        if (checkbox.checked) {
            let propPrice = prices.prodProperties[checkbox.name];
            if (propPrice !== undefined) {
                price += propPrice;
            }
        }
    });

    let prodPrice = document.getElementById("prodPrice");
    price = price * count.value;
    prodPrice.innerHTML = price + " рублей";
}

function getPrices() {
    return {
        prodTypes: [112, 73, 64],
        prodOptions: {
            option1: 0,
            option2: -43,
            option3: -12,
        },
        prodProperties: {
            prop1: 56,
            prop2: 80,
            prop3: 40,
        }
    };
}

window.addEventListener('DOMContentLoaded', function () {
    let s = document.getElementsByName("prodType");
    let select = s[0];
    select.addEventListener("change", function(event) {
        let target = event.target;
        console.log(target.value);
        updatePrice();
    });

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            let r = event.target;
            console.log(r.value);
            updatePrice();
        });
    });

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            let c = event.target;
            console.log(c.name);
            console.log(c.value);
            updatePrice();
        });
    });

    let count = document.getElementById("count");
    count.addEventListener("change", function(event) {
        let k = event.target;
        console.log(k.value);
        updatePrice();
    })

    updatePrice();
});


